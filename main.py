#!/usr/bin/python3
# main.py

import time
import comhard
from bot import Bot
import os

# check for existing config
if os.path.exists('config.txt'):
    pass
else:
    pass

#   Comhard Instance
c = comhard.Schedule()

#   Bot Instance
b = Bot()
b.start()

# call update() every hour and send update if available
while True:
    if c.update():
        b.send_schedule()
    time.sleep(3600)