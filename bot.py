#!/usr/bin/python3
# bot.py

import telegram
from telegram.ext import Updater
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
import logging, time, redis, os
from configparser import ConfigParser
from pathlib import Path, PureWindowsPath, PurePosixPath

class Bot(object):
    class Msg:
        # predefining messages
        welcome     =   ( "Hallo %s! Darf ich mich vorstellen?\n"
                        "Ich bin Dozentus Informatikus Maximus!\n\n"
                        "Ich werde regelmäßig für dich überprüfen, ob es einen neuen Stundenplan gibt und "
                        "dir diesen hier schicken!"
                        "\n\n"
                        "Aktuell gilt dieser Stundenplan:"
                        )
        update      =   "Es ist ein neuer Stundenplan verfügbar:"
        cantanswer  =   "Auf Nachrichten antworten kann ich leider noch nicht."
        
    class Sticker:
        # predefining stickers
        welcome     =   "CAACAgIAAxkBAAEBnihfvXVCnmdMczzTFo3rcbpi9ij1xQACvgADJQNSDwrA0aYECcLxHgQ"
        attention   =   "CAACAgIAAxkBAAEBnjZfvXbDq7NPEhfTe4bgOfP5YaK5GAACyQADJQNSD-zuumaYUqrHHgQ"
    
    def __init__(self):
        #   Config
        self.config = ConfigParser()
        self.config.read_file(open(r'config.txt'))
        
        self.token = self.config.get('Telegram', 'token')
        self.rPath = self.config.get('Redis','path')
        self.rdb = self.config.get('Redis', 'db')
        
        self.fileName = "US-FI 29.pdf"
        self.filePath = Path(self.config.get('General', 'directory') + "/" + self.fileName)
        
        # Convert paths depending on OS
        if os.name == 'posix':
            self.filePath = PurePosixPath(self.filePath)
        elif os.name == 'nt':
            self.filePath = PureWindowsPath(self.filePath)
        
        self.filePath = str(self.filePath)
        
        # Handler
        self.updater = Updater(token=self.token, use_context=True)
        self.dispatcher = self.updater.dispatcher
        
        # Instances
        self.bot = telegram.Bot(self.token)
        if self.rPath[:8] == 'redis://':
            self.r = redis.from_url('redis://localhost', db=self.rdb)
        elif self.rPath[-4:] == 'sock':
            self.r = redis.Redis(unix_socket_path=self.rPath, db=self.rdb)
            
        
        #  Logger
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
        self.logger = logging.getLogger("BOT")
            
    def h_start(self, update, context):
        user = update.message.from_user
        
        # Add user to DB
        self.r.set(user.name, user.id)
        
        # Logentry
        self.logger.info('New user ' + user.name + " with ID " + str(user.id))
        
        # Send welcome message
        self.bot.send_sticker(chat_id=update.effective_chat.id, sticker=self.Sticker.welcome)
        context.bot.send_message(chat_id=update.effective_chat.id, text=self.Msg.welcome %(user.name))
        with open(self.filePath,"rb") as file:
            context.bot.send_document(chat_id=update.effective_chat.id, document=file, filename=self.fileName)

    def h_echo(self, update, context):
        user = update.message.from_user
        
        # Logentry
        self.logger.info(user.name + " sent a message: " + update.message.text)
        
        # Answer to message
        context.bot.send_message(chat_id=update.effective_chat.id, text=self.Msg.cantanswer)
        
    def start(self):
        # Start handler
        self.echo_handler = MessageHandler(Filters.text & (~Filters.command), self.h_echo)
        self.start_handler = CommandHandler('start', self.h_start)
        self.dispatcher.add_handler(self.echo_handler)
        self.dispatcher.add_handler(self.start_handler)
        
        # Start updating
        try:
            self.updater.start_polling()
        except Exception as e:
            self.logger.error(e)

    def send_schedule(self):
        db = self.r.keys(pattern="*")
        
        # Logentry
        self.logger.info("Begin distributing new schedule:")
        
        for user in db:
            uid = self.r.get(user).decode("UTF-8")
            uname = user.decode('utf-8')
            
            # Send messages
            try:
                self.bot.send_sticker(chat_id=uid, sticker=self.Sticker.attention)
                self.bot.send_message(chat_id=uid, text=self.Msg.update)
                with open(self.filePath,"rb") as file:
                    self.bot.sendDocument(chat_id=uid, document=file, filename=self.fileName)
                
                # Logentry
                self.logger.info("... sent to: " + uid + " / " + uname)
            
            except telegram.TelegramError as e:
                self.logger.error(e + " -> " + uid + " / " + uname)
                

