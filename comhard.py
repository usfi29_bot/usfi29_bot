#!/usr/bin/python3
# comhard.py

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By 
import time, filecmp, os, logging
from pathlib import Path, PureWindowsPath, PurePosixPath
from configparser import ConfigParser

class Schedule(object):
    def __init__(self):
        #   Config
        self.config = ConfigParser()
        self.config.read_file(open(r'config.txt'))
        
        self.username = self.config.get('Comhard', 'username')
        self.pwd = self.config.get('Comhard', 'password')
        self.fileName = "US-FI 29.pdf"
        self.fileDir = self.config.get('General', 'directory')
        self.filePath = Path(self.fileDir + "/" + self.fileName)
        self.tempFile = Path(self.fileDir + "/tmp/" + self.fileName)
        self.tempDir = Path(self.fileDir + "/tmp")
        
        # Convert paths depending on OS
        if os.name == 'posix':
            self.filePath = PurePosixPath(self.filePath)
            self.tempDir = PurePosixPath(self.tempDir)
            self.tempFile = PurePosixPath(self.tempFile)
        elif os.name == 'nt':
            self.filePath = PureWindowsPath(self.filePath)
            self.tempDir = PureWindowsPath(self.tempDir)
            self.tempFile = PureWindowsPath(self.tempFile)
            
        self.filePath = str(self.filePath)
        self.tempDir = str(self.tempDir)
        self.tempFile = str(self.tempFile)
        
        # Configure browser
        self.fp = webdriver.FirefoxProfile()
        self.fp.set_preference("browser.download.folderList", 2)
        self.fp.set_preference("browser.download.manager.showWhenStarting", False)
        self.fp.set_preference("browser.download.dir", self.tempDir)
        self.fp.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf")
        self.fp.set_preference("pdfjs.disabled", True)
        self.options = Options()
        self.options.headless = True
        
        self.timeout = 20
        
        #Init logger
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
        self.logger = logging.getLogger("Comhard")

    def compare(self):
        # Compare files, return result and replace pdf on update
        if not os.path.exists(self.filePath):
            os.rename(self.tempFile, self.filePath)
            return False
        elif filecmp.cmp(self.filePath, self.tempFile):
            os.remove(self.tempFile)
            return True
        else:
            os.remove(self.filePath)
            os.rename(self.tempFile, self.filePath)
            return False

    def update(self):
        # Init browser instance and open login page
        browser = webdriver.Firefox(firefox_profile=self.fp, firefox_options=self.options)
        _step = WebDriverWait(browser, self.timeout)
        
        try:
            # open login page
            browser.get("https://comhard.iliasnet.de/login.php") 
            
            # login
            login_attempt = _step.until(EC.presence_of_element_located((By.XPATH, "//input[@type='submit']")))
        
            browser.find_element_by_id("username").send_keys(self.username)
            browser.find_element_by_id("password").send_keys(self.pwd)

            login_attempt.click()
            
            # move to page "Stundenpläne"
            _step.until(EC.presence_of_element_located((By.LINK_TEXT, "Stundenpläne"))).click()
            
            # download pdf for US-FI 29
            _step.until(EC.presence_of_element_located((By.LINK_TEXT, "US-FI 29"))).click()
            
            # wait for download to complete
            while not os.path.exists(self.tempFile):
                time.sleep(1)
        except Exception as e:
            self.logger.error(e)
        finally:
            # close the browser
            browser.close()
        
        #   return if update is available
        if not self.compare():
            self.logger.info("New schedule is available")
            return True
        else:
            self.logger.info("Schedule is still up to date")
            return False
